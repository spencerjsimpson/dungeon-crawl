package com.mycompany.app;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Server {
    public static void start() throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(3000), 0);
        server.createContext("/", new FileHandler());
        server.start();
        System.out.println("Server has been started for http://localhost:3000");
    }
}
